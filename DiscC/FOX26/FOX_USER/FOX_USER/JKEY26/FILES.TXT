Files included in JKEY25.EXE (self extracting .ZIP)

JKEY.DOC           Documentation
JKEY.NEW           Whats new since the previous version of jKey
JKEY.PLB           jKey for FoxPro/DOS 2.5
JKEY.FLL           jKey for FoxPro/WIN 2.5
JKEY20.PLB         jKey for FoxPro/DOS 2.0
JKEY.C             jKey source code
JKEYSAMP.PRG       jKey sample program
JDBLCLK.PRG        Program to show how to get Double Clicking to exit
                   BROWSE
FILES.TXT          This most exciting file

International Character Translation tables:

INT_D437.MEM       DOS Code Page 437   (Standard, US and Europe)
INT_D850.MEM       DOS Code Page 850   (Eastern Europe)
INT_D852.MEM       DOS Code Page 852   (SouthEastern Europe)
INT_ANSI.MEM       Windows ANSI        (Worldwide)

International translation tables were supplied by
"wOOdy" (Juergen Wondzinski, CIS ID: 100015,676)...
