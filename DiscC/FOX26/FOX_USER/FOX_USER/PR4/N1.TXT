旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
�           CPCURRENT()           �
읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
CPCURRENT([1])
컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
Returns the code page setting (if  any)  in  your  configuration  file,  or
returns the current operating system code page. Return value - Numeric
컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
CPCURRENT() returns one of the following:

 0 if the CODEPAGE configuration item isn't included in your configuration
  file.

 The code page number specified in the CODEPAGE  configuration  item.  For
example, CPCURRENT() returns 852 if the following line is included in  your
configuration file:

        CODEPAGE = 852

 The current operating system code page if you have included the following
line in your configuration file:

        CODEPAGE = AUTO

CPCURRENT(1) returns the current operating system code page, regardless  of
your configuration CODEPAGE setting.
                                                                                                                                                                                                     
