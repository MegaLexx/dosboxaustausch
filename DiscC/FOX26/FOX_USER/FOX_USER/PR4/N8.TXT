旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
�          IDXCOLLATE()           �
읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
IDXCOLLATE([<.cdx file>,] <expN1> [, <expN2> | <expC>])

컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
Returns the collation sequence for an index or index tag.
Return value - Character
컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

This function allows you to completely delete an index file and rebuild  it
correctly, using a series of SET COLLATE and INDEX commands. Note that this
function isn't required for the proper functioning of REINDEX, because  the
collation sequence information is present in  existing  indexes  and  index
tags.

<.cdx file>
        IDXCOLLATE() can be used to return the collation sequence for  each
        tag in multiple-entry compound index files. Specify the name of the
        compound index file with <.cdx file>. The compound index  file  you
        specify can be the structural  compound  index  file  automatically
        opened with the table or an independent compound index file.

<expN1>
        The numeric expression <expN1> specifies which index or  index  tag
        the collation sequence is returned for.  IDXCOLLATE()  returns  the
        collation sequence for indexes and  index  tags  in  the  following
        order:
        1. Collation sequences for single-entry.IDX index files (if any are
        open) are returned first. The order in which the single-entry index
        files are included in USE or SET INDEX determines how the collation
        sequences are returned.
        2. The collation sequences for  tags  in  the  structural  compound
        index  (if  one  is  present) are  returned  next.  The   collation
        sequences are returned for the tags in the order in which the  tags
        are created in the structural compound index.
        3.      Collation  sequences  for  tags  in  any  open  independent
        compound indexes are returned last.  The  collation  sequences  are
        returned for the tags in the order in which the tags are created in
        the independent compound indexes. The null string  is  returned  if
        <expN1> is greater than the total number of  open  single-entry.IDX
        files and structural compound and independent compound index tags.

<expN2> | <expC>
        If you omit the  work  area  and  alias,  collation  sequences  are
        returned from index files open for the table in  the  current  work
        area. To return collation sequences from  index  files  open  in  a
        specific work area, include the work area  number  <expN2>  or  the
        table alias <expC>. If no table has the alias you  specify,  FoxPro
        displays the message "Alias not found."
